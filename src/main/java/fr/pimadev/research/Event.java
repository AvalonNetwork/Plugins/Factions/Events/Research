package fr.pimadev.research;

import fr.pimadev.timerapi.Timer;
import fr.pimadev.timerapi.TimerListener;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Event {

    private Main           main;
    private Timer          timer;
    @Getter
    @Setter
    private int            basicsFossilsLeft;
    @Getter
    @Setter
    private int            fossilsLeft;
    @Getter
    private List<Location> fossilsLocations = new ArrayList<>();

    public Event(Main main) {
        this.main = main;
        this.basicsFossilsLeft = this.main.getBasicFossils();
        this.fossilsLeft = this.main.getFossils();
    }

    public void preStart() {
        this.timer = new Timer(this.main, 5, 0, this.main.PREFIX + "L'event commence dans §e§l" + Timer.LEFT_TIME + " §6! Utilisez la commande §e§l/research §6 pour voir les coordonnées des grottes où spawneront les minerais !", new TimerListener() {
            @Override
            public void onFinish() {
                Event.this.start();
            }
        });
    }

    public void start() {
        if (this.timer != null) {
            this.timer.stop();
        }
        Bukkit.broadcastMessage(this.main.PREFIX + "L'event commence !");
        this.placeOres();
    }

    public void stop() {
        if (this.timer != null) {
            this.timer.stop();
        }
        for (Location loc : this.fossilsLocations) {
            loc.getBlock().setType(Material.AIR);
        }
        this.main.setEvent(null);
    }

    private void placeOres() {
        Random random = new Random();
        for (int i = 0; i < this.main.getOtherOres(); i++) {
            Location location = this.getRandomLocation(random);
            location.getBlock().setType(Material.getMaterial(this.main.getOresIDs().get(random.nextInt(this.main.getOresIDs().size()))));
        }
        for (int i = 0; i < this.main.getBasicFossils(); i++) {
            Location location = this.getUniqueLocation(this.fossilsLocations, random);
            location.getBlock().setType(Material.getMaterial(this.main.getBlockBasicFossilID()));
        }
        for (int i = 0; i < this.main.getFossils(); i++) {
            Location location = this.getUniqueLocation(this.fossilsLocations, random);
            location.getBlock().setType(Material.getMaterial(this.main.getBlockFossilID()));
        }
    }

    private Location getRandomLocation(Random random) {
        return this.main.getOresLocations().get(random.nextInt(this.main.getOresLocations().size()));
    }

    private Location getUniqueLocation(List<Location> oldLocations, Random random) {
        Location location = this.getRandomLocation(random);
        while (oldLocations.contains(location)) {
            location = this.getRandomLocation(random);
        }
        oldLocations.add(location);
        return location;
    }

    public void blockBasicFossilMined() {
        this.basicsFossilsLeft--;
        Bukkit.broadcastMessage(this.main.PREFIX + "Il reste §e§l" + this.basicsFossilsLeft + " §6fossiles basiques !");
        this.checkFossilsLeft();
    }

    public void blockFossilMined() {
        this.fossilsLeft--;
        Bukkit.broadcastMessage(this.main.PREFIX + "Il reste §e§l" + this.fossilsLeft + " §6fossiles !");
        this.checkFossilsLeft();
    }

    public void checkFossilsLeft() {
        if (this.fossilsLeft <= 0 && this.basicsFossilsLeft <= 0) {
            Bukkit.broadcastMessage(this.main.PREFIX + "Tous les fossiles ont été miné !");
            this.stop();
        }
    }

}
