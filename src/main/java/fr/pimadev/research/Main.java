package fr.pimadev.research;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class Main extends JavaPlugin {

    public final String PREFIX                      = "§6";
    public final String CHOOSE_LOCATION_BLOCK_TITLE = "§eCliquez droit pour définir une location";

    @Getter
    private List<Location> cavesList;
    @Getter
    private List<Location> oresLocations = new ArrayList<>();
    @Getter
    private int            basicFossils;
    @Getter
    private int            fossils;
    @Getter
    private int            otherOres;
    @Getter
    private int            blockBasicFossilID;
    @Getter
    private int            blockFossilID;
    @Getter
    private List<Integer>  oresIDs;
    @Getter
    @Setter
    private Event          event;

    @Override
    public void onEnable() {
        super.onEnable();
        this.loadConfig();
        this.cavesList = this.parseLocations(this.getConfig().getStringList("caves_list"));
        this.oresLocations = this.parseLocations(this.getConfig().getStringList("ore_locations"));
        this.basicFossils = this.getConfig().getInt("basic_fossils_number");
        this.fossils = this.getConfig().getInt("fossils_number");
        this.otherOres = this.getConfig().getInt("other_ores_number");
        this.blockBasicFossilID = this.getConfig().getInt("basic_fossil_block_id");
        this.blockFossilID = this.getConfig().getInt("fossil_block_id");
        this.oresIDs = this.getConfig().getIntegerList("ores_ids");
        this.getCommand("research").setExecutor(new ResearchCommand(this));
        this.getServer().getPluginManager().registerEvents(new EventsListener(this), this);
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    private void loadConfig() {
        File config = new File(this.getConfig().getCurrentPath());
        if (!config.exists()) {
            this.getConfig().options().copyDefaults(true);
            this.saveConfig();
        }
    }

    public List<Location> parseLocations(List<String> serializedLocations) {
        List<Location> locations = new ArrayList<>();
        for (String serializedLoc : serializedLocations) {
            Location location = this.parseLoc(serializedLoc);
            if (!locations.contains(location)) {
                locations.add(location);
            }
        }
        return locations;
    }

    public List<String> serializeLocations(List<Location> locations) {
        List<String> serializedLocations = new ArrayList<>();
        for (Location loc : locations) {
            serializedLocations.add(this.serializeLocation(loc));
        }
        return serializedLocations;
    }

    public Location parseLoc(String serializedLocation) {
        String[] str   = serializedLocation.split(",");
        String   world = str[0];
        double   x     = Double.valueOf(str[1]).doubleValue();
        double   y     = Double.valueOf(str[2]).doubleValue();
        double   z     = Double.valueOf(str[3]).doubleValue();
        return new Location(Bukkit.getWorld(world), x, y, z);
    }

    public String serializeLocation(Location location) {
        return location.getWorld().getName() + "," + location.getBlockX() + "," + location.getBlockY() + "," + location.getBlockZ();
    }

    public void saveOreLocations() {
        this.getConfig().set("ore_locations", this.serializeLocations(this.oresLocations));
        this.saveConfig();
    }

}
