package fr.pimadev.research;

import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

@AllArgsConstructor
public class ResearchCommand implements CommandExecutor {

    private Main main;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("§cSeul un joueur peut executer cette commande.");
            return true;
        }
        Player p = (Player) sender;
        if (!p.isOp() || (args.length == 1 && args[0].equalsIgnoreCase("caves"))) {
            p.sendMessage(this.main.PREFIX + "Coordonnées des grottes où spawneront les minerais :");
            for (Location loc : this.main.getCavesList()) {
                p.sendMessage("§6X: §e§l" + loc.getBlockX() + " §6Y: §e§l" + loc.getBlockY() + " §6Z: §e§l" + loc.getBlockZ());
            }
        }
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("define_loc")) {
                ItemStack stack = new ItemStack(Material.DIAMOND_ORE);
                ItemMeta  meta  = this.getItemMeta(stack);
                meta.setDisplayName(this.main.CHOOSE_LOCATION_BLOCK_TITLE);
                stack.setItemMeta(meta);
                p.getInventory().addItem(stack);
                p.sendMessage("§aPosez ce bloc à chaque location que vous voulez définir comme étant location de spawn de minerai lors de l'event.");
            }
            else if (args[0].equalsIgnoreCase("start")) {
                if ((this.main.getBasicFossils() + this.main.getFossils()) > this.main.getOresLocations().size()) {
                    p.sendMessage("§cIl n'y a pas assez de locations de définit (il faut au moins autant de locations que de fossiles).");
                    return true;
                }
                if (this.main.getEvent() != null) {
                    p.sendMessage("§cIl y a déjà un event en cours.");
                    return true;
                }
                this.main.setEvent(new Event(this.main));
                this.main.getEvent().preStart();
            }
            else if (args[0].equalsIgnoreCase("stop")) {
                if (this.main.getEvent() == null) {
                    p.sendMessage("§cIl n'y a pas d'event en cours.");
                    return true;
                }
                Bukkit.broadcastMessage(this.main.PREFIX + "§cL'event vient d'être stoppé par un administrateur.");
                this.main.getEvent().stop();
            }
        }
        return true;
    }

    private ItemMeta getItemMeta(ItemStack stack) {
        return stack.hasItemMeta() ? stack.getItemMeta() : Bukkit.getItemFactory().getItemMeta(stack.getType());
    }

}
