package fr.pimadev.research;

import lombok.AllArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

@AllArgsConstructor
public class EventsListener implements Listener {

    private Main main;

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        if (!e.getPlayer().isOp()) {
            return;
        }
        if (e.getItemInHand() != null && e.getItemInHand().hasItemMeta() && e.getItemInHand().getItemMeta().getDisplayName() != null && e.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(this.main.CHOOSE_LOCATION_BLOCK_TITLE)) {
            e.setCancelled(true);
            e.getPlayer().sendMessage("§aVous venez de définir une nouvelle location de spawn de minerai pendant l'event à l'endroit où vous venez de placer le bloc.");
            if (!this.main.getOresLocations().contains(e.getBlock().getLocation())) {
                this.main.getOresLocations().add(e.getBlock().getLocation());
            }
            this.main.saveOreLocations();
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if (this.main.getEvent() != null && this.main.getEvent().getFossilsLocations().contains(e.getBlock().getLocation())) {
            if (e.getBlock().getType().getId() == this.main.getBlockBasicFossilID()) {
                this.main.getEvent().blockBasicFossilMined();
            }
            if (e.getBlock().getType().getId() == this.main.getBlockFossilID()) {
                this.main.getEvent().blockFossilMined();
            }
        }
    }

}
